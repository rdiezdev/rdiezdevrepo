package edu.iam.dam.ramon.ex12;

public class Alumne implements Ordenable, Imprimible {
	String nom, cognom, NIF, nacionalitat, password;
	int edat;
	char sexe;
	
	public Alumne(String n, String c, String NI){
		nom = n;
		cognom = c;
		NIF = NI;
		password=nom+cognom;
	}
	public void saluda(String nom){
		System.out.println("Hola, soc un alumne i em dic " + nom);
	}
	
	@Override
	public boolean menorQue(Ordenable ob) {
		return (this.edat < ((Alumne)ob).edat) ? true : false;
	}
	@Override
	public String imprimir() {
		return this.nom + " - edat: " + this.edat;
	}
}
