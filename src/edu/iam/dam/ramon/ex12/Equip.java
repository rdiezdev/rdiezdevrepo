package edu.iam.dam.ramon.ex12;

public class Equip implements Ordenable, Imprimible{
	private int puntsLliga;
	private String nomEquip;
	
	public Equip(String nomE){
		this.nomEquip= nomE;
		this.puntsLliga=0;
	}
	
	@Override
	public String toString(){
		return nomEquip;
	}

	public int getPuntsLliga() {
		return puntsLliga;
	}

	public void setPuntsLliga(int puntsLliga) {
		this.puntsLliga += puntsLliga;
	}

	public String getNomEquip() {
		return nomEquip;
	}

	@Override
	public boolean menorQue(Ordenable ob) {
		if(this.getPuntsLliga() < ((Equip)ob).getPuntsLliga())
			return true;
		else
			return false;
	}

	@Override
	public String imprimir() {
		return this.getNomEquip() + " - punts lliga: " + this.getPuntsLliga();
	}

}
