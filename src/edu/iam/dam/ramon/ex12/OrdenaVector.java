package edu.iam.dam.ramon.ex12;

import java.awt.image.ImageProducer;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class OrdenaVector {
	public static void directSort(Ordenable[] ord) {
		for (int i = 0; i < (ord.length - 1); i++) {
			for (int j = i + 1; j < ord.length; j++) {
				if (ord[i].menorQue(ord[j])) {
					Ordenable aux = ord[j];
					ord[j] = ord[i];
					ord[i] = aux;
				}
			}
		}
	}
}
