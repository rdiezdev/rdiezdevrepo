package edu.iam.dam.ramon.ex12;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ImprimirVectors {
	public static void imprimirConsola(Imprimible[] imprimibles) {
		for (Imprimible imp : imprimibles) {
			System.out.println(imp.imprimir());
		}
	}
	
	public static void imprimirAFitxer(Imprimible[] imprimibles) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter("arxiu.txt");
		for (Imprimible imprimirble : imprimibles) {
			pw.println(imprimirble.imprimir());
		}
		
		pw.close();
	}
}
