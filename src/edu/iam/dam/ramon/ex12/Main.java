package edu.iam.dam.ramon.ex12;

import java.io.FileNotFoundException;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		Alumne al1 = new Alumne("Ramon", "Esp", "11111");
		al1.edat = 1;
		
		Alumne al2 = new Alumne("Natalia", "Gonz", "22222");
		al2.edat = 25;
		
		Alumne al3 = new Alumne("Adr", "Samnos", "3333");
		al3.edat = 40;
		
		
		Equip ep1 = new Equip("Barça");
		ep1.setPuntsLliga(1);
		
		Equip ep2 = new Equip("Madrid");
		ep2.setPuntsLliga(20);
		
		Equip ep3 = new Equip("Valencia");
		ep3.setPuntsLliga(27);
		
		Equip equips[] = new Equip[3];
		equips[0] = ep1;
		equips[1] = ep2;
		equips[2] = ep3;
		
		
		Alumne alumnes[] = new Alumne[3];
		alumnes[0] = al1;
		alumnes[1] = al2;
		alumnes[2] = al3;
		
		OrdenaVector.directSort(equips);
		OrdenaVector.directSort(alumnes);
		
		
		//ORDENO
		for (Alumne alumne : alumnes) {
			System.out.println(alumne.edat);
		}
		for (Equip equip : equips) {
			System.out.println(equip.getPuntsLliga());
		}
		
		//IMPRIMEIXO PER CONSOLA I A FITXER
		ImprimirVectors.imprimirConsola(equips);
		ImprimirVectors.imprimirAFitxer(alumnes);
	}

}
