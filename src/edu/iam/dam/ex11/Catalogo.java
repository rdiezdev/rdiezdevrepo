package edu.iam.dam.ex11;

import java.lang.reflect.Array;

public class Catalogo {
	private Obra[] obras = new Obra[100];

	public boolean añadeObra(Obra ob) {

		if (this.buscaObra(ob.numero) == null) {
			for(int i = 0; i < this.obras.length; i++ )
				if(this.obras[i] == null) {
					this.obras[i] = ob;
					return true;
				}
		}
		return false;
	}

	public boolean eliminarObra(int num) {
		for (int i = 0; i < obras.length; i++) {
			if(this.obras[i] != null) {
				if (this.obras[i].getNumero() == num) {
					this.obras[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	public Obra buscaObra(int num) {
		if(this.obras.length > 0) {
			for (int i = 0; i < this.obras.length; i++) {
				if(this.obras[i] != null)
					if (this.obras[i].numero == num)
						return this.obras[i];
			}
		}
		return null;
	}

	public int superficie() {
		int superficie = 0;
		for (Obra ob : obras) {
			if (ob instanceof Pintura)
				superficie += ((Pintura) ob).getArea();
		}

		return superficie;
	}

	public int masAlta() {
		int num = -1, masAlta = 0;
		for (Obra ob : obras) {
			if (ob instanceof Escultura)
				if (((Escultura) ob).getAltura() > masAlta) {
					masAlta = ((Escultura) ob).getAltura();
					num = ((Escultura) ob).getNumero();
				}
		}
		return num;
	}

}
