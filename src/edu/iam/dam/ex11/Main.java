package edu.iam.dam.ex11;

import java.lang.reflect.Array;

public class Main {

	public static void main(String[] args) {
		Artista a1 = new Artista("Ramon", "Barcelona");
		Artista a2 = new Artista("Natalia", "Canet");
		Artista a3 = new Artista("Bobby", "India");
		
		Pintura p1 = new Pintura(10,10,"Lienzo", "Paisaje rojo",1, a1, 2006 );
		Pintura p2 = new Pintura(20,20,"Lienzo2", "Paisaje rojo2",2, a2, 2008 );
		
		Escultura e1 = new Escultura("Gres", 10, "Pensador", 3, a3, 2007);
		Escultura e2 = new Escultura("Gres2", 20, "Pensador2", 4, a2, 2008);
		
		Catalogo clg = new Catalogo();
		
		System.out.println(clg.añadeObra(e1));
		System.out.println(clg.añadeObra(e2));
		System.out.println(clg.añadeObra(p1));
		System.out.println(clg.añadeObra(p2));
		System.out.println(clg.añadeObra(p1)); //Devolverá false
		
		System.out.println(clg.buscaObra(3));
		System.out.println(clg.masAlta());
		System.out.println(clg.superficie());
		System.out.println(clg.eliminarObra(1));
		
		
		
	}

}
