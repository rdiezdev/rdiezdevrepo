package edu.iam.dam.ex11;

public class Artista {
	private String nombre;
	private String lugarNacimiento;
	
	public Artista(String nombre, String lugarNacimiento) {
		this.nombre= nombre + " ";
		this.lugarNacimiento = lugarNacimiento;
	}
	
	public boolean saltar() {
		return true;
	}
	
	@Override
	public String toString() {
		return "Artista [nombre: " + this. nombre + " LugarNacimiento: " + this.lugarNacimiento +"]";
		
	}
}
