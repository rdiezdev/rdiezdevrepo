package edu.iam.dam.ex11;

public class Escultura extends Obra {
	private String material;
	private int altura;

	public Escultura(String material, int altura, String titulo, int numero, Artista autor, int ano) {
		super(titulo, numero, autor, ano);
		this.material = material;
		this.altura = altura;
	}

	public String getMaterial() {
		return material;
	}

	public int getAltura() {
		return altura;
	}
	
	@Override
	public String toString() {
		return super.toString() 
				+ " [" + this.material + " " + this.altura + "]";
	}
	

}
