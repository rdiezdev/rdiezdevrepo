package edu.iam.dam.ex11;

public class Pintura extends Obra {
	public int alto;
	public int ancho;
	public String soporte;
	
	public Pintura(int alto, int ancho, String soporte, String titulo, int numero, Artista autor, int ano) {
		super(titulo, numero, autor, ano);
		this.alto = alto;
		this.ancho = ancho;
		this.soporte = soporte;
	}
	@Override
	public String toString() {
		return super.toString() 
				+ "[" + this.alto + "x" + this.ancho 
				+ this.soporte + "]";
	}
	public int getAlto() {
		return alto;
	}
	public int getAncho() {
		return ancho;
	}
	public String getSoporte() {
		return soporte;
	}
	
	public int getArea() {
		return this.alto * this.ancho;
	}
	
}
