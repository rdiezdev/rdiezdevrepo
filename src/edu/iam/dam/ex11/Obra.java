package edu.iam.dam.ex11;

public abstract class Obra {
	protected String titulo;
	protected int numero;
	protected Artista autor;
	protected int ano;
	
	public Obra(String titulo, int numero, Artista autor, int ano) {
		this.titulo = titulo;
		this.numero = numero;
		this.autor = autor;
		this.ano = ano;
	}
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getTitulo() {
		return titulo;
	}

	public Artista getAutor() {
		return autor;
	}

	public int getAno() {
		return ano;
	}

	@Override
	public String toString() {
		return "[" + this.titulo + " " 
				+ this.numero + " " + this.autor 
				+ " " + this.ano + "]";
	}
}
