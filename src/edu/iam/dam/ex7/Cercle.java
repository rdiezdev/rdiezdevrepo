package edu.iam.dam.ex7;

public class Cercle extends ObjecteGeometric {
	private int radi;

	public Cercle(int x, int y, int color, int radi) {
		super(x, y, color);
		this.radi = radi;

	}

	@Override
	public String toString() {
		return "Cercle:\nX = " + this.getX() 
				+ "\nY = " + this.getY() 
				+ "\nColor = " + this.getColor() 
				+ "\nRadi = " + this.getRadi();
	}

	public double perimetre() {
		return 2 * Math.PI * this.radi;
	}

	public double area() {
		return Math.PI * Math.pow(this.radi,2);
	}
	
	public int getRadi() {
		return this.radi;
	}

}
