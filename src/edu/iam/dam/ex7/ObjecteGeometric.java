package edu.iam.dam.ex7;

public class ObjecteGeometric {
	private int x, y, color;

	public ObjecteGeometric(int x, int y, int color) {
		this.x = x;
		this.y = y;
		this.color = color;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getColor() {
		return color;
	}

	@Override
	public String toString() {
		return "ObjecteGeometric:\nX = " + this.x 
				+ "\nY =" + this.y 
				+ "\nColor = " + this.color;
	}
}
