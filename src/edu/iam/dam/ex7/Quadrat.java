package edu.iam.dam.ex7;

public class Quadrat extends ObjecteGeometric {
	private int costat;

	public Quadrat(int x, int y, int color, int costat) {
		super(x, y, color);
		this.costat = costat;
	}
	
	@Override
	public String toString() {
		return "Quadrat:\nX = " + this.getX() 
				+ "\nY =" + this.getY() 
				+ "\nColor = " + this.getColor() 
				+ "\nCostats = " + this.getCostat();
	}
	
	public int getCostat() {
		return this.costat;
	}
	
	public int perimetre() {
		return 4 * getCostat();
	}
	
	public int area() {	
		return (int)Math.pow(getCostat(),2);
	}
}
