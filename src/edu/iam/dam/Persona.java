package edu.iam.dam;

public class Persona {
	public String nom, cognom;
	private String dni;
	private int edat;
	private boolean estatCivil;
	
	public Persona (String nom, String cognom, String dni, int edat){
		this.nom = nom;
		this.cognom = cognom;
		this.dni = dni;
		this.edat = edat;
		this.estatCivil=false;
	}
	
	@Override
	public String toString(){
		return "Nom: " + this.getNom() + " Cognom: " + this.getCognom() + " DNI: " + this.getDni();
	}
	
	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public boolean isEstatCivil() {
		return estatCivil;
	}

	public void setEstatCivil(boolean estatCivil) {
		this.estatCivil = estatCivil;
	}

	public String getNom() {
		return nom;
	}

	public String getCognom() {
		return cognom;
	}

	public String getDni() {
		return dni;
	}
}
