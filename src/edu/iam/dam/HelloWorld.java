package edu.iam.dam;

import java.util.Scanner;

public class HelloWorld {

	
	public static void main(String[] args) {
		final int MAX = 5;
		float[] vect1, vect2, vect3;
		int i, numeroMax;
		
		vect1 = new float[MAX];
		vect2 = new float[MAX];
		vect3 = new float[MAX];
		Scanner entrada = new Scanner(System.in);
		do{
			System.out.println("Introduce el tamaño de los vectores (entre 1 y " + MAX + "): ");
			numeroMax = entrada.nextInt();
		}while(numeroMax<1 || (numeroMax>MAX));
		
		for(i=0; i<numeroMax; i++){
			System.out.println("introduce el valor " + i + " del primer vector");
			vect1[i]=entrada.nextInt();
		}
		for(i=0; i<numeroMax; i++){
			System.out.println("introduce el valor " + i + " del segundo vector");
			vect2[i]=entrada.nextInt();
		}
		
		System.out.println("*** vector 3 es la suma de los 2 primeros vectores ***");
		for(i=0;i < numeroMax; i++){
			vect3[i]=vect1[i] + vect2[i];
			System.out.println("posicio " + i + " " + vect3[i]);
		}
	}

}
