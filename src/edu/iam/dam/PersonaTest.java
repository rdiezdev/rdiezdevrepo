package edu.iam.dam;

import java.util.Scanner;

public class PersonaTest {
	public static void main(String[] args){
		String nom, cognom, dni;
		int edat;
		boolean estatC;
		
		Scanner teclat = new Scanner(System.in);
		
		System.out.println("Introdueix nom: ");
		nom = teclat.next();
		
		System.out.println("Introdueix cognom: ");
		cognom = teclat.next();

		System.out.println("Introdueix dni: ");
		dni = teclat.next();
		
		System.out.println("Introdueix edat: ");
		edat = teclat.nextInt();
		
		System.out.println("Estat casat? (S/N): ");
		//System.out.println(teclat.next().toUpperCase().trim());
		if(teclat.next().toUpperCase().trim().compareTo("S")==0)
			estatC=true;
		else
			estatC=false; 

		Persona person = new Persona(nom, cognom,dni, edat);
		
		System.out.println("Hola " + nom + " " + cognom + ", tens " + edat + " anys.");
		System.out.println(person.getEdat());
		System.out.println(person.isEstatCivil());
	}
}
