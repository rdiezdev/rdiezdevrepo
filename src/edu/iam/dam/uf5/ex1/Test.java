package edu.iam.dam.uf5.ex1;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.Socket;

import java.net.UnknownHostException;

import java.util.Scanner;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.Sequence;


public class Test {
	public static void main(String[] args) {
		
		FileInputStream fis;
		try {
			fis = new FileInputStream("hola.txt");
		} catch (FileNotFoundException fe) {
			System.out.println("L'arxiu no existeix");
		}
		
		try {
			Socket soc = new Socket("localhost", 1234);
		} catch (UnknownHostException e1) {
			System.out.println("No s'ha pogut accedir al host especificat.");
		} catch (IOException e1) {
			System.out.println("Error al crear el socket.");
		}
		
		try {
			Font.createFont(1, new FileInputStream("font.ttf"));
		} catch (FileNotFoundException e1) {
			System.out.println("No s'ha trobat l'arxiu");
		} catch (FontFormatException e1) {
			System.out.println("Mal format.");
		} catch (IOException e1) {
			System.out.println("Error inesperat");
		}
		
		try {
			Sequence sq = new Sequence((float) 3.0, 3);
		} catch (InvalidMidiDataException e1) {
			System.out.println("El tipus de divisió no és vàlid.");
		}
		
		Scanner teclat = new Scanner(System.in);
		double a=0,b=0,c=0;
		boolean incorrect = true;
		while(incorrect) {
			try {
				System.out.println("Primer terme: ");
				a = Integer.parseInt(teclat.nextLine());
				
				System.out.println("Segon terme: ");
				b = Integer.parseInt(teclat.nextLine());
				
				System.out.println("Tercer terme: ");
				c = Integer.parseInt(teclat.nextLine());
				incorrect=false;
			}catch(NumberFormatException e) {
				System.out.println("Has d'introduir números.");
			}
		}
		
		EquacioGrau2 eq1 = new EquacioGrau2(a, b, c);
		
		try {
			eq1.arrels();
			System.out.println(eq1.getSol1());
			System.out.println(eq1.getSol2());
		} catch (PrimerCoeficientZeroException e) {
			System.out.println("ERROR - Divisio per zero");
		} catch (NoArrelsRealsException e) {
			System.out.println("ERROR - No hi ha solucions");
		}
		
	}

}
