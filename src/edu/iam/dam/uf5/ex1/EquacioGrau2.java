package edu.iam.dam.uf5.ex1;

public class EquacioGrau2 {

	private double a, b, c, sol1 = 0, sol2 = 0;

	public EquacioGrau2(double a, double b, double c) {

		this.a = a;
		this.b = b;
		this.c = c;
	}

	public void arrels() throws PrimerCoeficientZeroException, NoArrelsRealsException {
		if (a == 0)
			throw new PrimerCoeficientZeroException();
		else if (b * b < 4 * a * c)
			throw new NoArrelsRealsException();
		sol1 = (b + Math.sqrt(Math.pow(b, 2.0) - (4.0 * a * c))) / (2.0 * a);
		sol2 = (b - Math.sqrt(Math.pow(b, 2.0) - (4.0 * a * c))) / (2.0 * a);
	}

	public double getSol1() {
		return sol1;
	}

	public double getSol2() {
		return sol2;
	}
}
