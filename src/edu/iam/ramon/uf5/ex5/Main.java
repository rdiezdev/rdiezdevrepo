package edu.iam.ramon.uf5.ex5;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int opcio=1;
		Scanner teclat = new Scanner(System.in);
		HashMap<String, Persona> agenda = new HashMap<>();
		while(opcio!=6) {
			System.out.println("Menu");
			System.out.println("1- Afegir");
			System.out.println("2- Esborrar");
			System.out.println("3- Buscar per nif");
			System.out.println("4- Buscar per nom");	
			System.out.println("5- Llistar agenda");
			System.out.println("6- Sortir");
			System.out.println("Opcio: ");
			opcio = Integer.parseInt(teclat.nextLine());
			
			Persona pAux = new Persona();

			switch (opcio) {
			case 1:
				System.out.println("Nif: "); pAux.Nif = teclat.nextLine();
				System.out.println("Nom: "); pAux.nom = teclat.nextLine();
				System.out.println("Telèfon: "); pAux.tlf = teclat.nextLine();
				System.out.println("Any naixement: "); pAux.anyNaix = Integer.parseInt(teclat.nextLine());
				
				if(agenda.containsKey(pAux.Nif)) {
					System.out.println("La persona ja existia, s'ha modificat el telèfon.");
					agenda.get(pAux.Nif).tlf = pAux.tlf;
				}else 
					agenda.put(pAux.Nif, pAux);
				
				break;
			case 2:
				System.out.println("Nif: "); pAux.Nif = teclat.nextLine();
				
				if(agenda.containsKey(pAux.Nif)) {
					agenda.remove(pAux.Nif);
					System.out.println("Esborrat!");
				}
				else
					System.out.println("No existeix a l'agenda.");
				
				break;
			case 3:
				System.out.println("Nif: "); pAux.Nif = teclat.nextLine();
				
				pAux = agenda.get(pAux.Nif);
				if(pAux!=null) {
					System.out.println(pAux);
				}else
					System.out.println("No existeix a l'agenda");
				break;
			case 4:
				System.out.println("Nom: "); pAux.nom = teclat.nextLine();
				boolean notFound=true;
				//Recorrem l'agenda amb la collection
				for(Persona p : agenda.values())
					if(p.nom.equals(pAux.nom)) {
						System.out.println(p);
						notFound = false;
					}
				if(notFound)
					System.out.println("Aquesta persona no està a l'agenda");
				break;
			case 5:
				System.out.println("-- AGENDA --");
				//Un altre manera de recorre l'agenda
				for(Entry<String, Persona> p: agenda.entrySet()) {
					System.out.println(p.getValue());
				}
				break;
			case 6:
				System.out.println("Adeu!");
				break;
			default:
				System.out.println("opcio incorrecte");
			}
		}
		
		teclat.close();
	}

}
