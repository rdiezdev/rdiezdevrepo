package edu.iam.ramon.uf5.ex5;

public class Persona {
	String Nif, nom, tlf;
	int anyNaix;
	
	public Persona() {
		
	}
	public Persona(String nif, String nom, String tlf, int anyNaix) {
		Nif = nif;
		this.nom = nom;
		this.tlf = tlf;
		this.anyNaix = anyNaix;
	}
	
	@Override
	public String toString() {
		return Nif + " " + nom + " " + anyNaix + "\n"
				+ "Tlf: " + tlf;
	}

}
