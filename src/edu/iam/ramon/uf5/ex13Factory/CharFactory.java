package edu.iam.ramon.uf5.ex13Factory;

import javax.xml.parsers.FactoryConfigurationError;

import edu.iam.ramon.uf5.ex13Factory.chars.Alcalde;
import edu.iam.ramon.uf5.ex13Factory.chars.Arquer;
import edu.iam.ramon.uf5.ex13Factory.chars.Capella;
import edu.iam.ramon.uf5.ex13Factory.chars.Char;
import edu.iam.ramon.uf5.ex13Factory.chars.Infanteria;
import edu.iam.ramon.uf5.ex13Factory.weapons.Aixada;
import edu.iam.ramon.uf5.ex13Factory.weapons.Arc;
import edu.iam.ramon.uf5.ex13Factory.weapons.Pistol;
import edu.iam.ramon.uf5.ex13Factory.weapons.Shotgun;
import edu.iam.ramon.uf5.ex13Factory.weapons.Tiraxines;

public class CharFactory {

	private CharFactory() {
	}
	
	public static Char getChar(String clase, String Object) throws Exception, ClassNotFoundException {
		Char personatge;
		
		switch(clase.toUpperCase()) {
			case "ARQUER":
				personatge = new Arquer();
				break;
			case "INFANTERIA":
				personatge = new Infanteria();
				break;
			case "ALCALDE":
				personatge = new Alcalde();
				break;
			case "CAPELLA":
				personatge = new Capella();
				break;
			default:
				throw new ClassNotFoundException("Aquesta clase de personatge no existeix.");
		}
		switch (Object) {
		case "ARC":
			personatge.setObject(new Arc());
			break;
		case "PISTOLA":
			personatge.setObject(new Pistol());
			break;
		case "SHOTGUN":
			personatge.setObject(new Shotgun());
			break;
		case "TIRAXINES":
			personatge.setObject(new Tiraxines());
			break;
		case "AIXADA":
			personatge.setObject(new Aixada());
			break;
		}
		return personatge;
		
	}
	

}
