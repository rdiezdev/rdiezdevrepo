package edu.iam.ramon.uf5.ex13Factory;

import edu.iam.ramon.uf5.ex13Factory.chars.Char;

public class Principal {

	public Principal() {
		try {
			Char char1 = CharFactory.getChar("Arquer", "Arc");
			char1.life();
			char1.move(2, 5);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("No pots crear aquest tipus de personatge");
		}
	}

}
