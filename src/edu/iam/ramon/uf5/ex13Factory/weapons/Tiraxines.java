package edu.iam.ramon.uf5.ex13Factory.weapons;

public class Tiraxines implements Weapon {
	int numMans = 1;
	int municio= 0;
	public Tiraxines() {
		
	}

	@Override
	public int Shot() {
		municio--;
		return 1;
	}

	@Override
	public void reload() {
		municio += 10;

	}

	@Override
	public int getMans() {
		return numMans;
	}

}
