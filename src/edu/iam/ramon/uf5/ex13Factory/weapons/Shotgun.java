package edu.iam.ramon.uf5.ex13Factory.weapons;

public class Shotgun implements Weapon {
	int numMans = 2;
	int municio = 0;
	public Shotgun() {
	}

	@Override
	public int Shot() {
		if(municio >=2) {
			municio-=2;
			return 10;
		}
		return 0;
	}

	@Override
	public void reload() {
		municio+=10;

	}

	@Override
	public int getMans() {
		return numMans;
	}

}
