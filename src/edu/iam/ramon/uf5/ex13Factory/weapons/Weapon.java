package edu.iam.ramon.uf5.ex13Factory.weapons;

public interface Weapon {
	public int Shot();
	public void reload();
	public int getMans();
}
