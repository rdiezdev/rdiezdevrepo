package edu.iam.ramon.uf5.ex13Factory.chars;

import java.awt.Point;

import edu.iam.ramon.uf5.ex13Factory.weapons.Weapon;

public abstract class Villager implements Char {

	private int vides;
	private Point posicio;
	private Weapon maDreta, maEsquerra;
	public Villager() {
		posicio = new Point(0, 0);
		vides = 0;
	}

	@Override
	public void life() {
		vides = 1;
	}

	@Override
	public void move(int xSteps, int ySteps) {
		//Els pagesos es mouen més lents
		posicio.setLocation(xSteps-1, ySteps-1);

	}

	public void setObject(Weapon wp) throws Exception {
		if(wp.getMans()== 2) {
			if(maDreta != null) {
				 maDreta = wp;
			}else{
				throw new Exception("No pots portar aquest arma.");
			}
		}else {
			if(maDreta == null) {
				maDreta = wp;
			}else if(maEsquerra == null) {
				maEsquerra = wp;
			}else {
				throw new Exception("No pots portar aquest arma.");
			}
		}
	}

}
