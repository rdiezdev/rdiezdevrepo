package edu.iam.ramon.uf5.ex13Factory.chars;

import edu.iam.ramon.uf5.ex13Factory.weapons.Weapon;

public interface Char {
	
	public void life();
	public void move(int x, int y);
	public void setObject(Weapon wp) throws Exception;
	public Weapon getWeapon();
}
