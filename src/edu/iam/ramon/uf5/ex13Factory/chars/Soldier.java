package edu.iam.ramon.uf5.ex13Factory.chars;

import java.awt.Point;

import edu.iam.ramon.uf5.ex13Factory.weapons.Weapon;

public abstract class Soldier implements Char {
	private int vides;
	private Point posicio;
	private Weapon maDreta;
	private Weapon maEsquerra;
	
	public Soldier() {
		posicio = new Point(0, 0);
		vides = 0;
	}

	@Override
	public void life() {
		vides = 1;

	}

	@Override
	public void move(int xSteps, int ySteps) {
		//Els soldats es mouen més ràpid.
		posicio.setLocation(xSteps+2, ySteps+2);

	}

	
	public void setObject(Weapon wp) throws Exception {
		if(wp.getMans()== 2) {
			if(maDreta != null) {
				 maDreta = wp;
			}else{
				throw new Exception("No pots portar aquest arma.");
			}
		}else {
			if(maDreta == null) {
				maDreta = wp;
			}else if(maEsquerra == null) {
				maEsquerra = wp;
			}else {
				throw new Exception("No pots portar aquest arma.");
			}
		}
	}
	 

}
