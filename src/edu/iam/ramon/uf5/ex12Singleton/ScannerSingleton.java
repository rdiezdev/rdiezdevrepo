package edu.iam.ramon.uf5.ex12Singleton;

import java.util.Scanner;

public class ScannerSingleton {

	private static Scanner teclat = null;
	
	private ScannerSingleton() { }
	
	public static Scanner getInstance() {
		if(teclat==null)
			teclat = new Scanner(System.in);
		return teclat;
	}
}
