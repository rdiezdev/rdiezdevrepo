package edu.iam.ramon.uf5.ex6;

import java.io.Serializable;

public class Jugador implements Serializable, Comparable<Jugador>{
	int dorsal, edat, golsAcumulats;
	String nom, cognom;
	double alcada;
	
	public Jugador() {
		this.golsAcumulats= 0;
	}
	//FORMAT DEL FITXER
	//DORSAL#COGNOM#NOM#GOLSACUMULATS#EDAT#ALCADA
	public Jugador(String linea) throws AtributtesMissmatch {
		String dadesJugador[] = linea.split("#");
		
		if(dadesJugador.length != 6) 
			throw new AtributtesMissmatch("Formato de linea incorrecto. ");
		
		dorsal = Integer.valueOf(dadesJugador[0]);
		cognom = dadesJugador[1];
		nom = dadesJugador[2];
		golsAcumulats = Integer.valueOf(dadesJugador[3]);
		edat = Integer.valueOf(dadesJugador[4]);
		alcada = Double.valueOf(dadesJugador[5]);
	}
	
	@Override
	public String toString() {
		return dorsal + ": " 
				+ " Nom: " + cognom;
//				+ " Gols: " + golsAcumulats ;
				//+ " Edat: " + edat 
				//+ " Alçada: " + alcada;
	}

	@Override
	public int compareTo(Jugador o) {
		if(golsAcumulats > o.golsAcumulats)
			return -1;
		else if(golsAcumulats < o.golsAcumulats)
			return 1;
		return 0;
	}
	
	public String getStringFileFormat() {
		return dorsal + "#" + cognom + "#" 
				+ nom + "#" + golsAcumulats + "#" 
				+ edat + "#" + alcada;
	}

}
