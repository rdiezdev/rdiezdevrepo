package edu.iam.ramon.uf5.ex6;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Partit implements Serializable {
	private Equip equipLocal, equipVisitant;
	private int golsEquipLocal, golsEquipVisitant;
	public Map<Integer, Jugador> alineacioLocal, alineacioVisitant;
	public Map<Integer, Jugador> jugadorsMarcatLocal, jugadorsMarcatVisitant;

	public Partit(Equip eLocal, Equip eVisitant) {
		this.equipLocal = eLocal;
		this.equipVisitant = eVisitant;
		this.golsEquipLocal = 0;
		this.golsEquipVisitant = 0;
		alineacioLocal = eLocal.generarAlineacio();
		alineacioVisitant = eVisitant.generarAlineacio();
		jugadorsMarcatLocal = new HashMap<>();
		jugadorsMarcatVisitant = new HashMap<>();
		
	}

	public void marcaEquipLocal() {
		golsEquipLocal++;
		marcaJugador(0);
	}

	public void marcaEquipLocal(int gols) {
		golsEquipLocal = gols;
		for(int i = 0; i < gols; i++)
			marcaJugador(0);
		
	}

	public void marcaEquipVisitant() {
		golsEquipVisitant++;
		marcaJugador(1);
	}

	public void marcaEquipVisitant(int gols) {
		golsEquipVisitant = gols;
		for(int i = 0; i < gols; i++)
			marcaJugador(1);
	}

	// localOVistant 0: local 1: visitant
	private void marcaJugador(int localOVistant) {
		Random rnd = new Random();
		
		int indexAleatori = rnd.nextInt(11);
		int count =0;
		if(localOVistant == 0) {
			for (Jugador jug : alineacioLocal.values()) {
				if(indexAleatori == count) {
					Jugador jAux = alineacioLocal.get(jug.dorsal);
					jugadorsMarcatLocal.put(jAux.dorsal,jAux);
					jAux.golsAcumulats++;
				}
				count++;
			}
		}else {
			for (Jugador jug : alineacioVisitant.values()) {
				if(indexAleatori == count) {
					Jugador jAux = alineacioVisitant.get(jug.dorsal);
					jugadorsMarcatVisitant.put(jAux.dorsal,jAux);
					jAux.golsAcumulats++;
				}
				count++;
			}
		}
		
		
		
		
	}

	public String fi() {
		if (golsEquipLocal > golsEquipVisitant) {
			equipLocal.setPuntsLliga(3);
			return equipLocal.toString();
		}

		else if (golsEquipLocal < golsEquipVisitant) {
			equipVisitant.setPuntsLliga(3);
			return equipVisitant.toString();
		}
		equipLocal.setPuntsLliga(1);
		equipVisitant.setPuntsLliga(1);
		return "empat";
	}

	@Override
	public String toString() {
		return "Equip Local: " + equipLocal.getNomEquip() + " gols: " + golsEquipLocal + "\nEquip Visitant: "
				+ equipVisitant.getNomEquip() + " gols: " + golsEquipVisitant;
	}

	public int getGolsEquipLocal() {
		return golsEquipLocal;
	}

	public int getGolsEquipVisitant() {
		return golsEquipVisitant;
	}

	public Equip getEquipLocal() {
		return equipLocal;
	}

	public Equip getEquipVisitant() {
		return equipVisitant;
	}

}
