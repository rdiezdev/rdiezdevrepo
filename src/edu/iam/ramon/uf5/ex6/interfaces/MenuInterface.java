package edu.iam.ramon.uf5.ex6.interfaces;

import java.util.Hashtable;

import edu.iam.ramon.uf5.ex6.Equip;
import edu.iam.ramon.uf5.ex6.Jugador;
import edu.iam.ramon.uf5.ex6.Lliga;

public interface MenuInterface {

	/**
	 * Mostrarà el menú pincipal
	 * 
	 * @return Retorna la opció seleccionada per l´usuari
	 */
	int menuPrincipal();

	/**
	 * Demana a l´usuari les opcions per gestionar la lliga
	 * 
	 * @return la opció seleccionada
	 */
	int menuGestionarLliga(boolean modiEsboDisabled);

	/**
	 * Opcions per modicficar l´equip
	 * 
	 * @return la opció seleccionada
	 */
	int menuModificarEquip();

	/**
	 * Demana a l´usuari el nom del equip
	 * 
	 * @return el nou nom del equip
	 */
	String menuNouEquip();

	/**
	 * Per demanar tots els jugadors d´un equip
	 * 
	 * @return una hashtable amb els jugadors
	 */
	Hashtable<Integer, Jugador> demanarJugadors();

	/**
	 * Escull un equip de la liga
	 * 
	 * @param lliga
	 *            on estan els equips a mostrar el seu nom
	 * @return el equip escollit
	 */
	Equip escollirEquip(Lliga lliga);

	/**
	 * Demana les dades del jugador a crear
	 * 
	 * @return el jugador creat
	 */
	Jugador demanarJugador(Equip e);

	void eliminarJugador(Equip equip);

	String demanarNomEquip();

	Equip demanarEquip();

}