package edu.iam.ramon.uf5.ex6.interfaces;

import java.io.IOException;

import edu.iam.ramon.uf5.ex6.IsNotDirecotryException;

public interface ControladorInterface {

	void iniciarMenu();

	void jugarLliga() throws IOException;

	void gestionarLliga();

	void modificarEquip();

	void crearEquip();

	void eliminarEquip();

}