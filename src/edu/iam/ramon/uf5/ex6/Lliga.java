package edu.iam.ramon.uf5.ex6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import edu.iam.dam.uf5.ex1.EquacioGrau2;


public class Lliga implements Serializable{
	private final int JUGADORS_MINIMS = 15;
	private String nomLliga;
	List<Equip> equips  = new ArrayList<>();
	Vector<Partit> partits = new Vector();
	public String pathLliga;

	public Lliga(List<Equip> equips) {
		this.equips = equips;
		
		partits = new Vector<>();

		for (Equip equipLocal : equips) 
			for (Equip equipVisitant : equips) 
				if(!equipLocal.equals(equipVisitant)) 
					this.partits.addElement(new Partit(equipLocal,equipVisitant));				
	}
	
	
	public Lliga(String rutaLliga) throws IsNotDirecotryException, IOException {
		pathLliga = rutaLliga;
		
		if(!Paths.get(pathLliga).toFile().isDirectory()) {
			File f = new File(pathLliga.toString());
			if(!f.mkdirs()) {
				throw new IsNotDirecotryException("La ruta proporcionada no és vàlida o no hi ha permissos suficients.");
			}
		}
		
		nomLliga = Paths.get(pathLliga).getFileName().toString();
		
		//recorro los archivos para crear equipos
		File[] auxEquips = Paths.get(pathLliga).toFile().listFiles();
		for(int i = 0; i < auxEquips.length; i++) {
			if(!auxEquips[i].getName().equals("resultat.txt"))
				try {
					equips.add(new Equip(auxEquips[i]));
				}catch (Exception e) {
					System.out.println(e.getMessage());
				}
				
		}
		//creo el emparejamiento de partidos
		for (Equip equipLocal : equips) 
			for (Equip equipVisitant : equips) 
				if(!equipLocal.equals(equipVisitant)) 
					this.partits.addElement(new Partit(equipLocal, equipVisitant));		
		
	}
	
	public int getNumeroEquips() {
		return equips.size();
	}
	
	private void jugarPartit(Partit partit) {
		int gols;
		Random rnd = new Random();
		
		//GOLS LOCAL
		gols = rnd.nextInt(6);
		partit.marcaEquipLocal(gols);

		//GOLS VISITANT
		gols = rnd.nextInt(6);		
		partit.marcaEquipVisitant(gols);

		partit.fi();
	}

	public void jugaLliga() throws Minimum15PlayersNotReachedException, NoEquipsException {
		
		//Hi ha equips?
		if(getNumeroEquips() < 2) {
			throw new NoEquipsException("No hi ha equips suficients per jugar la lliga");
		}
		
		//Miro que hi hagin els jugadors minims per equip
		for(Equip e: equips)
			if(e.jugadors.size() < JUGADORS_MINIMS)
				throw new Minimum15PlayersNotReachedException(e.getNomEquip());
		
		//Jugo tots els partits
		for (Partit p : partits) 
			jugarPartit(p);	
		
		//Creo el fitxer de resultats
		
		PrintWriter pw;
		try {
			pw = new PrintWriter(new FileOutputStream(Paths.get(pathLliga).toFile().getAbsolutePath() + "/resultat.txt"));
		} catch (FileNotFoundException e1) {
			System.out.println("No s'ha pogut crear el fitxer de resultats de la lliga");
			e1.printStackTrace();
			return;
		}
		
		//ordeno els equips
		equips.sort(null);
		
		//com que està ordenat incremento l'equip que ha quedat a dalt de tot
		equips.get(0).incrementaLligaGuanyada();
		
		pw.println("Lliga: " + nomLliga);
		
		Jugador jMax = new Jugador();
		
		for (Equip equip : equips) {
			List<Jugador> jugs = new ArrayList<>(equip.jugadors.values());
			jugs.sort(null);
			if(jMax.golsAcumulats < jugs.get(0).golsAcumulats) {
				jMax = jugs.get(0);
			}
		}
		
		pw.println("Pichichi: "  + jMax.dorsal + " " +jMax.nom + " Gols: " + jMax.golsAcumulats); 
		pw.println("** Classificació **");
		pw.print(classificacio());
		
		pw.println("** Partits **");
		for (Partit partit : partits) {
			pw.println(partit.getEquipLocal() + " vs " + partit.getEquipVisitant()); 
			pw.println("Resultat: " + partit.getGolsEquipLocal() + " - " + partit.getGolsEquipVisitant());
			pw.println("Han marcat: ");
			for (Jugador j : partit.jugadorsMarcatLocal.values()) 
				pw.println("- " + j.dorsal + " " + j.nom + " " + j.cognom);
			
			for (Jugador j : partit.jugadorsMarcatVisitant.values()) {
				pw.println("- " + j.dorsal + " " + j.nom + " " + j.cognom);
			}
			pw.println();
			
			pw.println("Alineacio Local");
			for (Jugador j : partit.alineacioLocal.values()) 
				pw.println("- " + j.dorsal + " " + j.nom + " " + j.cognom);
			
			pw.println("Alineacio Vistant");
			for (Jugador j : partit.alineacioVisitant.values()) {
				pw.println("- " + j.dorsal + " " + j.nom + " " + j.cognom);
			}
			
			
		}
		
		pw.close();
	}
	
	public void guardarLligaFitxers() {
		Iterator<Equip> eIterator = equips.iterator();
		
		while(eIterator.hasNext()) {
			Utils.saveToFile(Paths.get(pathLliga), eIterator.next());
		}
	}
	public String classificacio() {
		String clasi = "";
		int count = 1;
		equips.sort(null);
		for (Equip equip : equips)
			clasi += count++ + " - " +  equip.getNomEquip() + ": " + equip.getPuntsLliga() + "\n";
		
		return clasi;
	}
	
	public boolean isEmpty() {
		return equips.size() > 0 ? false : true;
	}
	
	/**
	 * 
	 * @param nom nombre del equipo
	 * @return Equip o null si no existe
	 */
	public Equip getEquip(String nom) {
		nom = nom.toLowerCase().trim();
		for(Equip e : equips) {
			if(e.getNomEquip().toLowerCase().trim().equals(nom))
				return e;
		}
		
		return null;
	}
	
	
}
