package edu.iam.ramon.uf5.ex6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Path;

public class Utils {
	
	public static String sanejarStr(String s) {
		return s.replaceAll("\\W+", "-");
	}
	
	public static File equipToFile(Path path, Equip e) {
		return new File(path.toString()   + "/" + Utils.sanejarStr(e.getNomEquip()).toLowerCase() + e.EXTENSION);
	}
	
	public static boolean saveToFile(Path path, Equip e) {
		PrintWriter pw = null;
		
		try {
			
			pw = new PrintWriter(Utils.equipToFile(path, e));
			pw.println("nom:" + e.getNomEquip() + "," + "anyfundacio:" + e.getAnyFundacio() + "," + "lliguesguanyades:" + e.getLliguesGuanyades());
			for (Jugador j : e.jugadors.values())
				pw.println(j.getStringFileFormat());

		} catch (FileNotFoundException efnf) {
			System.out.println("L'arxiu especificat per l'equip no existeix.");
		}

		pw.close();
		return true;
	}
	
	public static boolean cambiarNomFitxerEquip(Path path, Equip e, String nouNom) {
		File fileNew = new File(path.toString(), Utils.sanejarStr(nouNom).toLowerCase() + Equip.EXTENSION);
		File original = equipToFile(path, e);
		if(!original.renameTo(fileNew))
			return false;
		return true;
	}
}
