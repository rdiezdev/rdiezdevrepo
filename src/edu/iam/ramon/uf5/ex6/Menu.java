package edu.iam.ramon.uf5.ex6;

import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;

import edu.iam.ramon.uf5.ex6.interfaces.MenuInterface;

public class Menu {
	Scanner teclat = new Scanner(System.in);
	int opcio;

	public int menuPrincipal() {
		System.out.println("1. Jugar Lliga");
		System.out.println("2. Gestionar Lliga");
		System.out.println("3- Exportar / Importar");
		System.out.println("4. Sortir");
		System.out.println("Opcio: ");
		opcio = Integer.valueOf(teclat.nextLine());

		return opcio;
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		teclat.close();
	}

	public int menuGestionarLliga(boolean modiEsboDisabled) {
		boolean opcioValida;

		do {
			opcioValida = true;

			System.out.println("1. Crear equip");
			if (modiEsboDisabled) {
				System.out.println("2. Modificar equip (Deshabilitat)");
				System.out.println("3. Esborrar equip (Deshabilitat)");
			} else {
				System.out.println("2. Modificar equip");
				System.out.println("3. Esborrar equip");
			}
			System.out.println("4. < --");
			System.out.println("Opcio: ");
			opcio = Integer.valueOf(teclat.nextLine());

			if (modiEsboDisabled && (opcio == 2 || opcio == 3))
				opcioValida = false;
		} while (!opcioValida || opcio < 1 || opcio > 4);

		return opcio;
	}

	public int menuModificarEquip(Equip e) {
		int opcio;
		do {
			System.out.println("------------------");
			System.out.println("- "+  e.getNomEquip());
			System.out.println("------------------");
			System.out.println("1. Modificar nom");
			System.out.println("2. Afegir jugador");
			System.out.println("3. Modificar jugador");
			System.out.println("4. Eliminar jugador");
			System.out.println("5. < --");
			System.out.println("Opcio: ");
			opcio=  Integer.valueOf(teclat.nextLine());	
		}while(opcio < 1 || opcio > 5);
		
		return opcio;
	}
	
	public int menuExportImport() {
		int opcio;
		
		do {
			System.out.println("1. Importar Natiu Java");
			System.out.println("2. Exportar Natiu Java");
			System.out.println("3. Importar JSON");
			System.out.println("4. Exportar JSON");
			System.out.println("5. < --");
			opcio = Integer.valueOf(teclat.nextLine());
		}while(opcio < 1 || opcio > 5);
		return opcio;
	}

	
	
	/**
	 * Demanar el jugador, si afegir es false et retorna el jugador en cas de que existeixi.
	 * 
	 * @param e Equip on afegir o modificar el jugador
	 * @param afegir true per afegir un jugador, false per a retorne el jugador que es busca
	 * @return retorna el jugador o null si no exiteix
	 */
	public Jugador demanarJugador(Equip e, boolean afegir) {
		Jugador jugador = null;
		String nomDorsal;
		
		if(!afegir) {
			System.out.println("Introdueix el nom del jugador o dorsal");
			nomDorsal = teclat.nextLine();
			try {
				int dorsal = Integer.valueOf(nomDorsal);
				return e.jugadors.get(dorsal);
			}catch(NumberFormatException nfe) {
				for(Jugador j : e.jugadors.values())
					if(j.nom.equals(nomDorsal))
						return j;
			}
		}else {
			jugador = new Jugador();
			System.out.println("------------------");
			System.out.println("- Afegir Jugador a "+  e.getNomEquip());
			System.out.println("------------------");
			System.out.println("Nom: "); jugador.nom = teclat.nextLine();
			System.out.println("Cognom: "); jugador.cognom = teclat.nextLine();
			System.out.println("Dorsal: "); jugador.dorsal = Integer.valueOf(teclat.nextLine());
			if(e.jugadors.get(jugador.dorsal)!=null)
				System.out.println("Aquest dorsal ja exiteix");
			
			System.out.println("Edat: "); jugador.edat = Integer.valueOf(teclat.nextLine());
			System.out.println("Alçada: "); jugador.alcada = Double.valueOf(teclat.nextLine());
		}
		
		return jugador;
	}

	
	public int eliminarJugador(Equip equip) {
		System.out.println(equip.jugadors);
		return demanarJugador(equip, false).dorsal;
	}

	
	public String demanarNomEquip(Equip e) {
		if(e != null)
			System.out.println("Nou nom de l'equip (" + e.getNomEquip() + "): ");
		else
			System.out.println("Nom de l'equip: ");
		return teclat.nextLine().trim();
	}

	public void mostrarEquips(List<Equip> eqs) {
		System.out.println(eqs);
	}
	public Equip demanarEquip() {
		return new Equip(demanarNomEquip(null));	
	}
	public Jugador menuModificarJugador(Jugador j) {
		String strAux;
		
		System.out.println("Nom: (" + j.nom + ")"); 
		if((strAux = teclat.nextLine()).length() > 0)
			j.nom = strAux;
		
		System.out.println("Cognom: (" + j.cognom + ")" ); 
		if((strAux = teclat.nextLine()).length() > 0)
			j.cognom = strAux;
		
		System.out.println("Dorsal: (" + j.dorsal + ")" );
		try {
			j.dorsal = Integer.valueOf(teclat.nextLine());
		}catch (NumberFormatException e) {}
		
		System.out.println("Edat: (" + j.edat + ")");
		try {
			j.edat = Integer.valueOf(teclat.nextLine());
		}catch (NumberFormatException e) {}
		
		System.out.println("Alçada: (" + j.alcada + ")");
		try {
			j.alcada = Double.valueOf(teclat.nextLine());
		}catch (NumberFormatException e) {}
		
		return j;
	}
	
	public String obrirArxiu() {
		System.out.println("Indica l'arxiu: ");
		return teclat.nextLine();
	}
	
	public String guardarArxiu() {
		System.out.println("Indica la carpeta on vols guardar l'arxiu");
		return teclat.nextLine();
	}
}
