package edu.iam.ramon.uf5.ex6;

public class Minimum15PlayersNotReachedException extends Exception {
	

	public Minimum15PlayersNotReachedException(String equip) {
		super("No es pot jugar la lliga ja que l'equip ' " + equip + "' no te els 15 jugadors mínims necessaris.");
	}


}
