package edu.iam.ramon.uf5.ex6;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import com.google.gson.Gson;

import edu.iam.ramon.uf5.ex6.interfaces.ControladorInterface;

public class Controller {
	private Menu menu;
	private Scanner teclat = new Scanner(System.in);
	private Lliga lliga;

	/**
	 * @param ruta
	 *            es la ruta de la liga, se le puede pasar para no haberla de
	 *            introducir por teclado
	 */
	public Controller(String ruta) {

		try {
			if (ruta == null) {
				System.out.println("Introdueix la ruta de la lliga");
				ruta = teclat.nextLine().trim();
			} else {
				if (!new File(ruta).isDirectory()) {
					File f = new File(ruta);
					if (!f.mkdirs()) {
						throw new IsNotDirecotryException();
					}
				}
			}
			
			lliga = new Lliga(ruta); 
			while (iniciarMenu() == 1);
		} catch (IsNotDirecotryException e1) {
			System.out.println("La ruta no és un directori vàlid.");
		} catch (IOException e1) {
			System.out.println("Hi ha hagut un error inesperat.");
			e1.printStackTrace();
		}

	}

	public int iniciarMenu() {
		menu = new Menu();
		switch (menu.menuPrincipal()) {
		case 1: // JUGAR LLIGA
			jugarLliga();
			break;
		case 2: // GESTIONAR LLIGA
			while(gestionarLliga());
			break;
		case 3: // EXPORTAR IMPORTAR LLIGA
				while(exportImportLliga());
			break;
		case 4: // SORTIR
			System.out.println("Adeu!");
			return 0;
		}
		return 1;
	}

	public void jugarLliga() {
		try {
			lliga.jugaLliga();
			lliga.guardarLligaFitxers();
			System.out.println("Lliga juada, pots veure el resultat al fitxer resultat.txt");
		} catch (Minimum15PlayersNotReachedException | NoEquipsException e) {
			if(e instanceof Minimum15PlayersNotReachedException)
				System.out.println(e.getMessage());
			else if (e instanceof NoEquipsException)
				System.out.println(e.getMessage());
		}
	}

	public boolean gestionarLliga() {

		switch (menu.menuGestionarLliga(lliga.isEmpty())) { // Si no hi ha equips no mostro totes les opcions
		case 1:
			crearEquip();
			break;
		case 2:
			modificarEquip();
			break;
		case 3:
			eliminarEquip();
			break;
		case 4:
			return false;
		}
		return true;
		

	}
	
	public boolean exportImportLliga(){
		Path path = Paths.get("");
		switch(menu.menuExportImport()) {
		case 1: //Import Natiu
			while(!path.toFile().isFile()) {
				path = Paths.get(menu.obrirArxiu());
			}
			try {
				ObjectInputStream in = new ObjectInputStream(new FileInputStream(path.toFile()));
				lliga = (Lliga) in.readObject();
				in.close();
			} catch (ClassNotFoundException | IOException e1) {
				System.out.println("Error quan s'ha volgut importar l'arxiu.");
			}
			break;
		case 2: //Export Natiu
			while(!path.toFile().isDirectory()) {
				path = Paths.get(menu.guardarArxiu());
			}
			ObjectOutputStream out;
			try {
				out = new ObjectOutputStream(new FileOutputStream(path.toString() + "/LaLLiga.natiu"));
				out.writeObject(lliga);
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error quan s'ha volgut guardar l'arxiu.");
			}
			
			break;
		case 3: //Importar JSON
			while(!path.toFile().isFile()) {
				path = Paths.get(menu.obrirArxiu());
			}
			try {
				Gson gson = new Gson();
				lliga = gson.fromJson(new FileReader(path.toFile()), Lliga.class);
			} catch ( IOException e1) {
				System.out.println("Error quan s'ha volgut importar l'arxiu.");
			}
			
			break;
		case 4: //Exportar JSON
			while(!path.toFile().isDirectory()) {
				path = Paths.get(menu.guardarArxiu());
			}
			try {
				Gson gson = new Gson();
				FileWriter fw = new FileWriter(path.toFile()+ "/LaLliga.json", false);
				fw.write(gson.toJson(lliga));
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 5:
			return false;
		}
		return true;
	}
	
	/**
	 *  CRUD Equip
	 */
	public void crearEquip() {
		String nom = menu.demanarNomEquip(null);
		Equip e = lliga.getEquip(nom);
		
		if(e!=null) 
			System.out.println("L'equip ja existeix.");
		else {
			e = new Equip(nom);
			lliga.equips.add(e);
			Utils.saveToFile(Paths.get(lliga.pathLliga), e);
		}
	}
	
	public void modificarEquip() {
		int opcio;

		// mostro els equips
		menu.mostrarEquips(lliga.equips);

		// busco l'equip
		Equip equip = lliga.getEquip(menu.demanarNomEquip(null));
		if (equip == null) {
			System.out.println("Lequip no existeix.");
			return;
		}

		// mostro el menu de modificar equip
		do {

			opcio = menu.menuModificarEquip(equip);

			switch (opcio) {
			case 1: // 1. Canviar nom equip i guardo el fitxer
				String nom = menu.demanarNomEquip(equip);
				if(nom.trim().length() > 0) {
					Utils.cambiarNomFitxerEquip(Paths.get(lliga.pathLliga), equip, nom);
					equip.setNomEquip(nom.trim());
				}
				
				break;
			case 2: // 2. Afegir jugador");
				afegirJugador(equip);
				break;
			case 3: // 3. Modificar jugador
				modificarJugador(equip);
				break;
			case 4: // 4. Eliminar jugador
				//TODO muestra mal la info de los jugadores
				equip.jugadors.remove(menu.eliminarJugador(equip));
				break;
			case 5: // Enrere...
				break;
			}
			Utils.saveToFile(Paths.get(lliga.pathLliga), equip);
		} while (opcio != 5);

	}
	
	public void eliminarEquip() {
		System.out.println(lliga.equips);
		
		Equip e = lliga.getEquip(menu.demanarNomEquip(null));
		if(e == null) {
			System.out.println("L'equip introduit no existeix.");
		}else {
			try {
				Utils.equipToFile(Paths.get(lliga.pathLliga), e).delete();
				lliga.equips.remove(e);
			} catch (SecurityException ex) {
				System.out.println("L'arxiu de l'equip introduit no existeix.");				
			}
		}
		
	}
	
	/**
	 * 
	 * CRUD Jugador
	 * 
	 */
	
	public void afegirJugador(Equip equip) {
		Jugador jugador;
		while ((jugador = menu.demanarJugador(equip, true)) != null) {
			
			equip.jugadors.put(jugador.dorsal, jugador);

			System.out.println("Vols introduir mes jugadors? (S/N)");
			if (teclat.nextLine().toLowerCase().equals("n"))
				break;
		}
	}
	
	/**
	 * 
	 * @param equip
	 * @return el jugador modificat o null
	 */
	public Jugador modificarJugador(Equip equip) {
		Jugador j = menu.demanarJugador(equip, false);
		if (j == null)
			System.out.println("El jugador no existeix.");
		else {
			 j = menu.menuModificarJugador(j);
		}
		return j;
	}
	
	

	

	

}
