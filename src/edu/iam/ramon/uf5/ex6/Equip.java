package edu.iam.ramon.uf5.ex6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.rmi.CORBA.Util;

public class Equip implements Comparable<Equip>, Serializable {
	public final static String EXTENSION = ".equip";
	private int puntsLliga, lliguesGuanyades, anyFundacio ;
	private String nomEquip;
	public HashMap<Integer, Jugador> jugadors = new HashMap<>();

	public Equip(String nomE) {
		this.nomEquip = nomE;
		this.puntsLliga = 0;
	}

	public Equip(File nomFitxer) throws Exception {
		int totalJugadors = 0;
		this.puntsLliga = 0;
		lliguesGuanyades=0; //Ho especifico ja que no son paràmetres importants si en el fitxer no s'incolou
		anyFundacio=2000;	//El mateix comentari que lliguesGuanyades

		BufferedReader br;
		
		br = new BufferedReader(new FileReader(nomFitxer));
		
		String atributs[] = br.readLine().split(",");
		
		for(int i = 0; i < atributs.length ; i++) {
			String atribut[] = atributs[i].split(":");
			
			switch(atribut[0].toLowerCase().trim()) {
				case "nom": 
						if(atribut[1].length() > 0) 
							setNomEquip(atribut[1]);
						else {
							throw new Exception("L'atribut nom del fitxer no és correcte.");
						}
					break;
				case "anyfundacio":
					setAnyFundacio(Integer.valueOf(atribut[1]));
					break;
				case "lliguesguanyades":
					lliguesGuanyades = Integer.valueOf(atribut[1]);
					break;
			}
			
		}
		
		if(this.getNomEquip().length() == 0) {
			br.close();
			throw new AtributtesMissmatch("El nom de l'equip no està especificat a l'arxiu. Format esperat nom:nomEquip");
		}
		
		String auxLinea;
		while ((auxLinea = br.readLine()) != null) {
			try {
				Jugador auxJugador = new Jugador(auxLinea);
				jugadors.put(auxJugador.dorsal, auxJugador);
				totalJugadors++;
			} catch (AtributtesMissmatch e) {
				System.out.println("Equip: " + this.nomEquip + " - Conté linea mal formada ('" + auxLinea + "')");
				e.printStackTrace();
			}
		}

		br.close();

	}

	@Override
	public String toString() {
		return nomEquip;
	}

	public int getPuntsLliga() {
		return puntsLliga;
	}

	public void setPuntsLliga(int puntsLliga) {
		this.puntsLliga += puntsLliga;
	}

	public String getNomEquip() {
		return nomEquip;
	}
	
	public int getLliguesGuanyades() {
		return lliguesGuanyades;
	}

	public void incrementaLligaGuanyada() {
		this.lliguesGuanyades++;
	}

	public int getAnyFundacio() {
		return anyFundacio;
	}

	public void setAnyFundacio(int anyFundacio) {
		this.anyFundacio = anyFundacio;
	}

	@Override
	public int compareTo(Equip o) {

		if (this.puntsLliga > o.puntsLliga)
			return -1;
		else if (this.puntsLliga < o.puntsLliga)
			return 0;
		return 0;
	}

	public HashMap<Integer, Jugador> generarAlineacio() {
		Random rnd = new Random();
		HashMap<Integer, Jugador> alineacio = new HashMap<>();

		for (int i = 0; alineacio.size() != 11; i++) {
			int posRnd = rnd.nextInt(jugadors.size());

			int count = 0;
			for (Jugador jug : jugadors.values()) {
				if (count == posRnd) {
					if (!alineacio.containsKey(jug.dorsal))
						alineacio.put(jug.dorsal, jug);
					break;
				}
				count++;
			}
		}
		return alineacio;
	}
	
	public void setNomEquip(String nom) {
		this.nomEquip = nom.trim();
	}
	
	@Override
	public boolean equals(Object obj) {
		return this.nomEquip.toLowerCase().equals(((Equip)obj).getNomEquip().toLowerCase());
	}

}
