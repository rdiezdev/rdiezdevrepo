package edu.iam.ramon.uf5.ex3;

import java.util.Collection;
import java.util.Comparator;
import java.util.Random;
import java.util.Scanner;
import java.util.Vector;

public class TestVector {

	public static void main(String[] args) {
		Random rnd = new Random();

		// MODUL
		Vector<Double> v = new Vector<Double>();
		for (int i = 0; i < 10; i++) 
			v.add(rnd.nextDouble()*100);
		System.out.println(calcularModul(v));

		// NOTA ALUMNES
		Vector<Double> notes = new Vector<Double>();
		for (int i = 0; i < 10; i++) 
			notes.add(rnd.nextDouble()*10);
		mostrarNotes(notes);
		
		//DESCOMPONDRE VECTORS
		Vector<Integer> nums = new Vector<Integer>();
		Vector<Integer> vParells = new Vector<Integer>();
		Vector<Integer> vSenars = new Vector<Integer>();
		for (int i = 0; i < 20; i++)
			nums.add(rnd.nextInt(100));
		
		descompondreVector(nums,vParells, vSenars);
		System.out.println("PARELLS: " + vParells.toString());
		System.out.println("SENARS: " + vSenars.toString());
		
		// MODUL
		Vector<Integer> valors = new Vector<Integer>();
		for (int i = 0; i < 20; i++) 
			valors.add(rnd.nextInt(100));
		valors.sort(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				if(o1>o2)
					return 1;
				else if(o1<o2)
					return -1;
				return 0;
			}
		});
		System.out.println("Vector Ordenat");
		System.out.println(valors.toString());
		
	}

	private static double calcularModul(Collection<Double> nums) {
		double total = 0;
		for (Double num : nums)
			total += Math.pow(num, 2);
		return Math.sqrt(total);
	}

	private static void mostrarNotes(Collection<Double> grup) {
		int insufi=0, aprovat=0, notable=0, excelent=0;
		for (Double nota : grup) {
			if(nota >=0 && nota <5)
				insufi++;
			else if(nota >=5 && nota <7)
				aprovat++;
			else if(nota >=7 && nota <9)
				notable++;
			else if(nota >=9 && nota <=10)
				excelent++;
		}
		
		System.out.println("Insuficients: " + insufi);
		System.out.println("Aprovats: " + aprovat);
		System.out.println("Notables: " + notable);
		System.out.println("Excelents: " + excelent);
	}
	
	private static void descompondreVector(	Collection<Integer> nums, 
											Collection<Integer> parells, 
											Collection<Integer> senars) {
		for (Integer num : nums) {
			if(num%2==0)
				parells.add(num);
			else
				senars.add(num);
		}
	}

}
