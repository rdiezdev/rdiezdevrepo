package edu.iam.ramon.uf5.ex3;

public class Estudiant {
	private String nom;
	int edat, nota;

	public Estudiant(String nom, int edat, int nota) {
		this.nom = nom;
		this.edat = edat;
		this.nota = nota;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public String getNom() {
		return nom;
	}

	public String toString() {
		return "Nom: " + nom + "\nEdat: " + edat + "\nNota: " + nota;
	}
}