package edu.iam.ramon.uf5.ex3;

public class Professor {
	String nom;
	int edat;
	
	public Professor(String nom, int edat){
		this.nom=nom;
		this.edat=edat;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public String getNom() {
		return nom;
	}
	
	public String toString(){
		return "Nom: " + nom;
	}
	
}