package edu.iam.ramon.uf5.ex3;

import java.util.Iterator;
import java.util.Vector;

public class Grup {
	private String nom;
	private Vector<Estudiant> estudiants;
	private Professor profe;

	public Grup(String nom, Vector<Estudiant> estudiants, Professor profe) {
		this.nom = nom;
		this.estudiants = estudiants;
		this.profe = profe;
	}

	public String toString() {
		String grup = "--GRUP " + this.nom + "--\n";
		Iterator<Estudiant> iter= estudiants.iterator();
		while(iter.hasNext())
			grup+=(iter.next() + "\n\n");
		return grup;
	}

	public int numAprovats() {
		int total = 0;
		Iterator<Estudiant> iter= estudiants.iterator();
		while(iter.hasNext())
			if(iter.next().nota>=5);
				total++;
		return total;
	}

	public Vector<Estudiant> grupAprovats() {
		Vector<Estudiant> eAprovats = new Vector<Estudiant>();
		Iterator<Estudiant> iter= this.estudiants.iterator();
		while(iter.hasNext()) {
			Estudiant e = iter.next();
			if(e.nota >=5)
				eAprovats.add(e);
		}
		return estudiants;
	}
}