package edu.iam.ramon.uf5.ex11JSON;

import java.io.Serializable;


public class Professor {

	private String nom;
	private int edat;
	private  int sou;
	
	public Professor() {}
	public Professor(String nom, int edat) {
		this.nom = nom;
		this.edat = edat;
		this.sou = 0; 
	}
	
	public Professor(String nom, int edat, int sou) {
		this.nom = nom;
		this.edat = edat;
		this.sou = sou;
	}
	
	public void complirAnys() {
		edat = edat + 1;
	}
	
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	public int getSou() {
		return sou;
	}
	public void setSou(int sou) {
		this.sou = sou;
	}
	public void imprimir() {
		System.out.println("Nom: " + nom + "   Edat: " + edat + "   Sou: " + sou);
	}

}
