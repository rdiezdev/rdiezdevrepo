package edu.iam.ramon.uf5.ex11JSON;

import java.io.Serializable;

public class Alumne {
	
	private String nom;
	private int edat;
	private float nota;
	private Professor tutor;
	
	public Alumne() {
		
	}
	public Alumne(String nom, int edat) {
		this.nom = nom;
		this.edat = edat;
		nota = 10;
	}
	
	public Alumne(String nom, int edat, float nota) {
		this.nom = nom;
		this.edat = edat;
		this.nota = nota;
	}
	
	public void complirAnys() {
		edat = edat + 1;
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public float getNota() {
		return nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}

	public Professor getTutor() {
		return tutor;
	}

	public void setTutor(Professor tutor) {
		this.tutor = tutor;
	}

	public void imprimir() {
		System.out.println("Nom: " + nom + "   Edat: " + edat + "   Nota: " + nota + "   Tutor: " + tutor.getNom());
	}

}
