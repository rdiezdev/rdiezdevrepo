package edu.iam.ramon.uf5.ex11JSON;

import java.io.Serializable;
import java.util.Scanner;



public class Grup {


	
	// L'únic atribut del grup és un vector d'objectes de classe Alumne
	private Alumne alumnes[];


	
	// Constructor
	
	public Grup(Alumne[] als) {
		this.alumnes = als;
	}
	public Grup() {
	};



	public Alumne[] getAlumnes() {
		return alumnes;
	}
	public void setAlumnes(Alumne[] alumnes) {
		this.alumnes = alumnes;
	}
	// métode "numAprovats" retorna el número d'alumnes aprovats
	public int numAprovats() {

		int n = 0;	// número d'aprovats
		int i;		// per recòrrer el vector

		// recorrem el vector d'alumnes i obtenim la nota amb el mètode getNota() d'Alumne
		for (i = 0; i < alumnes.length; i++)
			if (alumnes[i].getNota() >= 5)
				n++;

		return n;
	};



	// métode "setTutor" assigna un tutor a un grup d'alumnes
	public void setTutor(Professor p) {

		int i;

		// Recorre el vector d'alumnes, i a cada alumne crida el seu mètode setTutor()
		for (i = 0; i < alumnes.length; i++)
			alumnes[i].setTutor(p);
	}



	// métode "imprimir" per imprimir un grup d'alumnes
	public void imprimir() {

		int i;

		System.out.println("Alumnes");
		System.out.println("-------");

		// Recorre el vector d'alumnes, i a cada alumne crida el seu mètode imprimir()
		for (i = 0; i < alumnes.length; i++)
			alumnes[i].imprimir();
	}

}
