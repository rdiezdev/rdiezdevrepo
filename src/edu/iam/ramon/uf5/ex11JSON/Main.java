package edu.iam.ramon.uf5.ex11JSON;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;

public class Main {

	//Si que podem serialitzar objectes que dintre tenen objectes.

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		Random rnd = new Random();
		Alumne[] alumnes = new Alumne[10];
		Professor tutor = new Professor("Albert", 37);
		
		Gson gson = new Gson();
		
		//ToJson objecte per objecte
		for (int i = 0; i < 10; i++) {
			Alumne a = new Alumne("Alumne " + i, 30 + i, rnd.nextInt(11));
			alumnes[i] = a;
			a.setTutor(tutor);
			FileWriter fw = new FileWriter("/home/ausias/Escriptori/alumnes/" + a.getNom() + ".json", false);
			fw.write(gson.toJson(a));
			fw.close();
		}
		
		//ToJson array
		FileWriter fw = new FileWriter("/home/ausias/Escriptori/alumnes/alumnes.json", false);
		fw.write(gson.toJson(alumnes));
		fw.close();
		
		//ToJson object amb array
		Grup grp = new Grup(alumnes);
		fw = new FileWriter("/home/ausias/Escriptori/alumnes/grup.json", false);
		fw.write(gson.toJson(grp));
		fw.close();
		
		System.out.println("**** JSON OBJECT ******");
		//FromJson object
		Alumne al = gson.fromJson(new FileReader("/home/ausias/Escriptori/alumnes/Alumne 1.json"), Alumne.class);
		al.imprimir();
		
		System.out.println("**** JSON ARRAY ******");
		//FromJson array
		alumnes = new Alumne[10];
		alumnes = gson.fromJson(new FileReader("/home/ausias/Escriptori/alumnes/alumnes.json"), Alumne[].class);
		for (Alumne alumne : alumnes)
			alumne.imprimir();
		
		System.out.println("**** JSON OBJECT AMB ARRAY ******");
		grp = null;
		grp = gson.fromJson(new FileReader("/home/ausias/Escriptori/alumnes/grup.json"),Grup.class);
		grp.imprimir();
		
	}

}
