package edu.iam.ramon.uf5.ex9;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

public class Main {

	//Si que podem serialitzar objectes que dintre tenen objectes.

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		Random rnd = new Random();
		Alumne[] alumnes = new Alumne[10];
		Professor tutor = new Professor("Albert", 37);
		
		for (int i = 0; i < 10; i++) {
			Alumne a = new Alumne("Alumne " + i, 30 + i, rnd.nextInt(11));
			a.setTutor(tutor);
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("/home/ausias/alumnes/" + a.getNom()));
			out.writeObject(a);
			out.close();
		}
		
		Path path = Paths.get("/home/ausias/alumnes");
		int count=0;
		
		for(File fl: path.toFile().listFiles()) 
			if(fl.isFile()) {
				ObjectInputStream in = new ObjectInputStream(new FileInputStream(fl));
				alumnes[count++] = (Alumne) in.readObject();
				
				in.close();
			}
		
		for(Alumne al : alumnes) 
			System.out.println(al.getNom());
		
		Grup grp = new Grup(alumnes);
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("/home/ausias/grup/grup.bin"));
		out.writeObject(grp);
		out.close();
		grp = null;
		
		
		ObjectInputStream in = new ObjectInputStream(new FileInputStream("/home/ausias/grup/grup.bin"));
		grp = (Grup) in.readObject();
		in.close();
		
		grp.imprimir();

	}

}
