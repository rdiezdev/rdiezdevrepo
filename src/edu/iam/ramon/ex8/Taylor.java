package edu.iam.ramon.ex8;

public class Taylor {
	private static String nomAutor = "Ramon Espana Diez";
	
	public static double sinus(double num) {
		return (num - Math.pow(num,3)/3 + Math.pow(num,5)/120);
	}
	
	public static double cosinus(double num) {
		 return (1 - Math.pow(num,2)/2 + Math.pow(num,4)/24);
	}

	public static double exponencial(double num) {
		return (1 + num + Math.pow(num,2)/2 + Math.pow(num,3)/6 + Math.pow(num,4)/24 + Math.pow(num,5)/120);
	}
	
	public static String autor() {
		return nomAutor;
	}

}
