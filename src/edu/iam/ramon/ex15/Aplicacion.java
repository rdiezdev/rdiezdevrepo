package edu.iam.ramon.ex15;

import java.util.Scanner;

public class Aplicacion {
	
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		
		int opcio;
		do {
			Jugable joc = EligeJuego();
			joc.muestraNombre();
			joc.muestraInfo();
			joc.juega();
			System.out.println("Vols jugar un altre cop?");
			System.out.println("1 - SI");
			System.out.println("2 - NO");
			opcio = Integer.parseInt(teclat.next());
		}while(opcio==1);
		System.out.println("Adeu!!");
		
		
	}
	
	public static Jugable EligeJuego() {
		Scanner sc= new Scanner(System.in);
		int numUs;
		Jugable jocs[] = new Jugable[3];
		
		jocs[0] = new JuegoAdivinaImpar(3, 7);
		jocs[1] = new JuegoAdivinaPar(3, 10);
		jocs[2] = new JuegoAdivinaNumero(3, 2);
		
		do {
			System.out.println("Quin joc vols jugar?\n");
			System.out.println("0 - Impars ");
			System.out.println("1 - Parells ");
			System.out.println("2 - Normals");
			System.out.println("Opcio: ");
			numUs=Integer.parseInt(sc.next());
		}while(numUs>2 || numUs < 0);
		
		return jocs[numUs];
	}
}
