package edu.iam.ramon.ex15;

public abstract class Juego {
	private int numVidas, numVidasIniciales;
	private static int record = 0;
	
//	public abstract void Juega();
	
	public Juego(int numVidas) {
		this.numVidas = numVidas;
		this.numVidasIniciales = numVidas;
	}

	public void muestraVidasRestantes() {
		System.out.println("Numero vidas: " + this.numVidas);
	}

	public boolean QuitaVida() {
		if ((--this.numVidas) > 0)
			return true;
		System.out.println("Juego Terminado");
		return false;
	}

	public void ReiniciaPartida() {
		this.numVidas = this.numVidasIniciales;
	}
	
	public void ActualizaRecord() {
		if(this.numVidas == record) {
			System.out.println("Record alcanzado");
		}else if(this.numVidas > record) {
			System.out.println("Record superado");
			record = this.numVidas;
		}
	}
}
