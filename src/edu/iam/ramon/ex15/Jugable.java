package edu.iam.ramon.ex15;

public interface Jugable {
	public void juega();
	public void muestraNombre();
	public void muestraInfo();
}
