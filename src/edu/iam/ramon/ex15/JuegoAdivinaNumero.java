package edu.iam.ramon.ex15;

import java.util.Scanner;

public class JuegoAdivinaNumero extends Juego implements Jugable{
	private int numAdivinar;
	Scanner teclado = new Scanner(System.in);
	
	public JuegoAdivinaNumero(int numVidas, int numAdivinar) {
		super(numVidas);
		this.numAdivinar = numAdivinar;
	}

	@Override
	public void juega() {
		int numUsuari;
		ReiniciaPartida();
		
		
		do {
			do {
				System.out.println("Introdueix numero: ");
				numUsuari = teclado.nextInt();
			}while(!ValidaNumero(numUsuari));
			
			if(numUsuari == this.numAdivinar) {
				System.out.println("Acertaste!!");
				ActualizaRecord();
				break;
			}else {
				System.out.println("Sigue intentandolo...");
			}
		}while(QuitaVida()==true);
	}
	
	public boolean ValidaNumero(int numUsuari) {
		return true;
	}

	@Override
	public void muestraNombre() {
		System.out.println("Adivina un numero");
	}

	@Override
	public void muestraInfo() {
		System.out.println("Adivina un numero del 1 al 10");
	}
	
}
