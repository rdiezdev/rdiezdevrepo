package edu.iam.ramon.ex15;

public class JuegoAdivinaPar extends JuegoAdivinaNumero {

	public JuegoAdivinaPar(int numVidas, int numAdivinar) {
		super(numVidas, numAdivinar);
	}
	
	@Override
	public boolean ValidaNumero(int numUsuari) {
		if(numUsuari%2==0)
			return true;
		System.out.println("Ha de ser un numero par.");
		return false;
	}
	
	@Override
	public void muestraNombre() {
		System.out.println("Adivina numero Par");
	}
	@Override
	public void muestraInfo() {
		System.out.println("Numeros pares");
	}
	
}
