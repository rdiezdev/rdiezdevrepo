package edu.iam.ramon.ex15;

public class JuegoAdivinaImpar extends JuegoAdivinaNumero {

	public JuegoAdivinaImpar(int numVidas, int numAdivinar) {
		super(numVidas, numAdivinar);
	}
	@Override
	public boolean ValidaNumero(int numUsuari) {
		if(numUsuari%2!=0)
			return true;
		System.out.println("Ha de ser un numero impar.");
		return false;
	}
	
	@Override
	public void muestraNombre() {
		System.out.println("Adivina numero impar");
	}
	
	@Override
	public void muestraInfo() {
		System.out.println("adivina un numero impar");
	}
}
