package edu.iam.ramon.examen.A;

import java.io.Serializable;
import java.util.HashMap;

public class Cataleg extends HashMap<String, Producte> implements Serializable{

	
	public Producte cercaProducte(String model) {
		return this.get(model);
	}
	
	public boolean afegeixProducte (Producte p) throws Exception {
		
		if(this.get(p.model) != null) {
			throw new AlreadyExistsException("El producte ja existeix al catàleg.");
		}
		this.put(p.model, p);
		return true;
	}
	
	public boolean eliminaProducte(String model) {
		
		if(this.remove(model)== null) {
			return false;
		}
		
		return true;
		
	}
}
