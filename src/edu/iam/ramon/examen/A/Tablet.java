package edu.iam.ramon.examen.A;

import java.util.Date;

public class Tablet extends Producte {
	public boolean hasSim;
	
	public Tablet(String nom, String marca, String model, double preu, Date dataLlançament, boolean hasSim) {
		super(nom, marca, model, preu, dataLlançament);
		
		this.hasSim = hasSim;
	}
	
}
