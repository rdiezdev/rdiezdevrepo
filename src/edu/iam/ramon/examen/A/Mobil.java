package edu.iam.ramon.examen.A;

import java.util.Date;

public class Mobil extends Producte {
	public int numSims;
	
	public Mobil(String nom, String marca, String model, double preu, Date dataLlançament, int numSims) {
		super(nom, marca, model, preu, dataLlançament);
		this.numSims = numSims;
	}


}
