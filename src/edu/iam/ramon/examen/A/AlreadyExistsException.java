package edu.iam.ramon.examen.A;

public class AlreadyExistsException extends Exception {

	public AlreadyExistsException(String string) {
		super(string);
	}

}
