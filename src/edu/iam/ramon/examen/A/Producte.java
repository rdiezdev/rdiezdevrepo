package edu.iam.ramon.examen.A;

import java.io.Serializable;
import java.util.Date;

public abstract class Producte implements Serializable {

	public String nom, marca, model, numSerie;
	public double preu;
	public Date dataLlançament;
	
	public Producte(String nom, String marca, String model, double preu, Date dataLlançament) {
		this.nom = nom;
		this.marca = marca;
		this.model = model;
		this.preu = preu;
		this.dataLlançament = dataLlançament;
		
		generarNumSerie();
	}
	
	public void generarNumSerie( ) {
		numSerie = marca.toLowerCase()
				.replace('a', '4')
				.replace('i', '1')
				.replace('o', '0') + 
				model.toLowerCase()
				.replace('a', '4')
				.replace('i', '1')
				.replace('o', '0'); 
	}

}
