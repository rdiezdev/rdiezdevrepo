package edu.iam.ramon.ex13;

public class Main {

	public static void main(String[] args) {

			Movable m1 = new MovablePoint(5, 6, 10, 10);     // upcast
			System.out.println(m1);
			m1.moveLeft();
			System.out.println(m1);
			   
			Movable m2 = new MovableCircle(2, 1, 2, 20, 20); // upcast
			System.out.println(m2);
			m2.moveRight();
			System.out.println(m2);
			
			Movable m3 = new MovableRectangle(10, 10, 0, 0, 10, 5); //UPCAST
			System.out.println(m3);
			m3.moveRight();
			System.out.println(m3);	
			
			/*Ara, escriu una altre classe que es diu MovableRectangle, que està composta 
			de dosMovablePoints (representant els dos vèrtex dreta-abaix, esquerra-adalt)
			i que  implementi la interfície Movable. Assegura't que els dos punts es mouen
			amb la mateixa velocitat. Crea una classe que ho provi.*/
	}

}
