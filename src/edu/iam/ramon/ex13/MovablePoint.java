package edu.iam.ramon.ex13;

public class MovablePoint implements Movable {
	protected int x, y, xSpeed, ySpeed;
	
	
	public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
		this.x = x;
		this.y = y;
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;
	}
	
	@Override
	public void moveUp() {
		this.y += 1*ySpeed;
	}

	@Override
	public void moveDown() {
		this.y -= 1*ySpeed;
	}

	@Override
	public void moveLeft() {
		this.x -= 1* this.xSpeed;
	}

	@Override
	public void moveRight() {
		this.x += 1* this.xSpeed;
	}
	@Override
	public String toString() {
		return ("x: " + this.x + " // y: " + this.y);
	}
}
