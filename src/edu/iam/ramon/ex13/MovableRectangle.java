package edu.iam.ramon.ex13;

public class MovableRectangle implements Movable {
	MovablePoint puntDretInf, puntEsqSup;
	
	public MovableRectangle(int x1, int y1, int x2, int y2, int xSpeed, int ySpeed) {
		this.puntDretInf = new MovablePoint(x1, y1, xSpeed, ySpeed);
		this.puntEsqSup = new MovablePoint(x2, y2, xSpeed, ySpeed);
	}
	@Override
	public void moveUp() {
		this.puntDretInf.moveUp();
		this.puntEsqSup.moveUp();
	}

	@Override
	public void moveDown() {
		this.puntDretInf.moveDown();
		this.puntEsqSup.moveDown();
	}

	@Override
	public void moveLeft() {
		this.puntDretInf.moveLeft();
		this.puntEsqSup.moveLeft();
	}

	@Override
	public void moveRight() {
		this.puntDretInf.moveRight();
		this.puntEsqSup.moveRight();
	}
	
	@Override
	public String toString() {
		return puntDretInf + " -- " + puntEsqSup;
	}

}
