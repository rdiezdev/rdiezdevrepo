package edu.iam.ramon.ex16;

public class BotigaMain {
	public static Magatzem magatzemBCN;
	
	public static void main(String[] args) {
		
		Canco[] canconsFito = new Canco[] {
				new Canco("1. Antes de que cuente diez", 3,20),
				new Canco("2. Crazy", 3,15),
				new Canco("3. La casa por el tejado", 2,55)
		};
		
		Canco[] canconsAero = new Canco[] {
				new Canco("1. Dream on", 4,20),
				new Canco("2. Soldadito Marinero", 4,15),
				new Canco("3. I don't want miss a thing", 4,55)
		};
		
		Article[] articles = new Article[] {
				new Dvd (3, "Un ingenyier es venja del sistema ...",19.90, 50, "Un ciudadano ejemplar", 
						"Almodovar", "Castellà", 1, 50, 37),
				new Cd(4, "Cd de Fito", 9.90, 100, "Fito Tour", "Fito y Fitipaldis", canconsFito),
				new Cd(5, "Cd de Aerosmith", 19.90, 3, "Aeromisth Live", "Aerosmith", canconsAero),
				new Llibre(1, "Un llibre de programació." , 34.50 , 2, "Think Java", "autor", 540),
				new Llibre(2, "Llibre de dibuix.", 12.50, 10, "Dibuixa jugant", "autor dibuixar", 30)
				
		};
		
		magatzemBCN = new Magatzem(articles);
		double preuAux;

		Article artAux = magatzemBCN.buscarProducte(3);
		
		//REALITZEM UNA VENTA
		if((preuAux = artAux.vendre(5)) != -1) {
			System.out.println("VENTA");
			System.out.println(artAux);
			System.out.println("Preu Total : " + preuAux + " €");
		}else {
			System.out.println("No hi ha suficient stock");
		}
		
		//REALITZEM UNA VENTA AMB STOCK INSUFICIENT
		artAux = magatzemBCN.buscarProducte(4);
		if((preuAux = artAux.vendre(500)) != -1) {
			System.out.println("VENTA");
			System.out.println(artAux);
			System.out.println("Preu Total : " + preuAux + " €");
		}else {
			System.out.println("No hi ha suficient stock");
		}
		
		
		//REPOSTEM D'STOCK UN PRODUCTE
		magatzemBCN.buscarProducte(3).repostar(25);
		
		//MOSTREM CONTINGUT (DVD I CD)
		magatzemBCN.mostrarProducte(3); //No mostrarà contingut
		magatzemBCN.mostrarProducte(4);
		
		//MOSTEM EL TOTAL DE CANÇONS
		System.out.println("Total de cançons: " + Cd.totalCancons);
		
		//AFEGIM ARTICLE JA EXISTENT
		if(magatzemBCN.afegirArticle(new  Llibre(2, "Llibre de dibuix.", 12.50, 10, "Dibuixa jugant", "autor dibuixar", 30))) {
			System.out.println("Article Afegit.");
		}else {
			System.out.println("Aquest article ja existeix.");
		}
		
		//AFEGIM ARTICLE (NOU)
		if(magatzemBCN.afegirArticle(new  Llibre(7, "Llibre de Cant.", 12.50, 1, "Com cantar òpera", "autor cant", 30))) {
			System.out.println("Article Afegit.");
		}else {
			System.out.println("Aquest article ja existeix.");
		}
		
		
		//DEIXEM UN ARTICLE SENSE STOCK
		magatzemBCN.buscarProducte(7).vendre(1);
		
		//MOSTREM ARTICLES SENSE STOCK
		Article[] artStock = magatzemBCN.articlesSenseStock();
		if (artStock.length > 0) {
			System.out.println("Articles OUT OF STOCK");
			for (Article article : artStock) {
				System.out.println(" ** " + article);
			}
		}else {
			System.out.println("No hi ha articles sense stock");
		}
		
		
		//MOSTEM CONTINGUT MAGATZEM (DESORDENAT)
		System.out.println("-- CONTINGUT DESORDENAT --");
		System.out.println(magatzemBCN.contingutMagatzem());
		
		//MOSTEM CONTINGUT MAGATZEM (ORDENAT)
		System.out.println("-- CONTINGUT ORDENAT --");
		System.out.println(magatzemBCN.contingutMagatzemOrdenat());
		
	}

}
