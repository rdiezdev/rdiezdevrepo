package edu.iam.ramon.ex16;

public class Canco {
	private String canco;
	private int minuts, segons;
	
	public Canco(String canco, int minuts, int segons) {
		this.canco = canco;
		this.minuts = minuts;
		this.segons = segons;
	}
	
	@Override
	public String toString() {
		return canco + " " + minuts + ":" + segons;
	}
	

}
