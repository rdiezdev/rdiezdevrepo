package edu.iam.ramon.ex16;

public class Cd extends Article implements Mostrable {
	private String titol, nomBanda;
	public Canco[] cancons;
	public static int totalCancons = 0;
	
	public Cd(int codi, String descripcio, double preu, int stock, String titol, String nomBanda, Canco[] cancons) {
		super(codi, descripcio, preu, stock);
		this.titol = titol;
		this.nomBanda = nomBanda;
		this.cancons = cancons;
		
		totalCancons += this.cancons.length;
	}

	@Override
	public String MostrarContingut() {
		String contingut;
		contingut = this + 
					"- Banda: " + this.nomBanda + "\n" +
					"- Títol: " + this.titol + "\n" + 
					"- Cançons- \n";
		for (Canco canco : cancons) {
			contingut += canco + "\n";
		}
		return contingut;
	}
	
	@Override
	public String toString() {
		return "Codi: " + getCodi() + " Títol: " + this.titol + "\n";
	}

}
