package edu.iam.ramon.ex16;

public abstract class Article implements Comparable<Article> {
	private int stock;
	private int codi;
	private String descripcio;
	private double preu;
	
	public Article(int codi, String descripcio, double preu, int stock) {
		this.codi = codi;
		this.descripcio = descripcio;
		this.preu = preu;
		this.stock = stock;
	}
	
	public double vendre(int quantiat) {
		if(quantiat <= this.stock) {
			this.stock-=quantiat;
			
			//Si ja no hi ha stock d'aquest CD resto 
			//del total de cancons
			if(this instanceof Cd && this.stock == 0) {
				Cd.totalCancons -= ((Cd)this).cancons.length;
			}
			return this.preu * quantiat;
		}
		return -1;
	}
	
	public void repostar(int quantitat) {
		this.stock+=quantitat;
	}
	
	@Override
	public int compareTo(Article o) {
		if(o.getCodi() > this.getCodi())
			return -1;
		else if(o.getCodi() < this.getCodi())
			return 1;
		return 0;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getCodi() {
		return codi;
	}

	public void setCodi(int codi) {
		this.codi = codi;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getPreu() {
		return preu;
	}

	public void setPreu(float preu) {
		this.preu = preu;
	}	

}
