package edu.iam.ramon.ex16;

public class Dvd extends Article {
	private String titol, director, idioma;
	private int hores, minuts, segons;
	
	public Dvd(int codi, String descripcio, double preu, int stock, String titol, String director, String idioma,
			int hores, int minuts, int segons) {
		super(codi, descripcio, preu, stock);
		this.titol = titol;
		this.director = director;
		this.idioma = idioma;
		this.hores = hores;
		this.minuts = minuts;
		this.segons = segons;
	}
	
	@Override
	public String toString() {
		return "Codi: " + this.getCodi() + " Títol: " + this.titol + "\n"; 
	}
	
	
}
