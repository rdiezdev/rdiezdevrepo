package edu.iam.ramon.ex16;

public class Llibre extends Article implements Mostrable{
	private String titol, autor;
	private int numPagines;
	
	public Llibre(int codi, String descripcio, double preu, int stock, String titol, String autor, int numPagines) {
		super(codi, descripcio, preu, stock);
		this.titol = titol;
		this.autor = autor;	
		this.numPagines = numPagines;
	}

	@Override
	public String MostrarContingut() {
		return	this +
				"- Autor: " + this.autor + "\n" +
				"- Títol: " + this.titol + "\n" + 
				"- Pàgines: " + this.numPagines;
	}
	
	@Override
	public String toString() {
		return "Codi: " + this.getCodi() + " Títol: " + this.titol +"\n";
	}

}
