package edu.iam.ramon.ex16;

import java.util.Arrays;

public class Magatzem {
	public Article[] articles;
	
	public Magatzem(Article[] art) {
		this.articles = art;
	}
	
	
	public boolean afegirArticle(Article art) {
		Article[] artsAux = new Article[this.articles.length+1];
		
		for (Article article : articles)
			if(article.getCodi() == art.getCodi())
				return false;
		
		for (int i = 0; i < articles.length; i++)
			artsAux[i] = articles[i]; 
		
		artsAux[articles.length] = art;
		articles = artsAux;
		
		return true;
	}
	
	public Article buscarProducte(int codi) {
		
		for (Article article : articles) 
			if(article.getCodi() == codi) 
				return article;
		
		return null;
	}
	
	public void mostrarProducte(int codi) {
		if(buscarProducte(codi) instanceof Mostrable) {
			System.out.println(((Mostrable)buscarProducte(codi)).MostrarContingut());
		}else {
			System.out.println("L'article no te contingut a mostrar.");
		}
	}
	
	public Article[] articlesSenseStock() {
		int count=0;
		
		//primer miro quants hi ha sense stock
		for (Article article : articles)
			if(article.getStock() == 0) 
				count++;
		
		Article[] artAux = new Article[count];
		count=0;
		//primer miro quants hi ha sense stock
		for (int i = 0; i < articles.length; i++)
			if(articles[i].getStock() == 0) 
				artAux[count++] = articles[i];
		
		return artAux;
	}
	
	
	public String contingutMagatzem() {
		String strAux = "";
		for (Article article : articles) {
			strAux += article;
			/*if(article instanceof Mostrable)
				strAux += ((Mostrable)article).MostrarContingut();*/
		}
		
		return strAux;
	}
	
	public String contingutMagatzemOrdenat() {
		String strAux = "";
		Arrays.sort(articles);
		
		for (Article article : articles) {
			strAux += article;
			/*if(article instanceof Mostrable)
				strAux += ((Mostrable)article).MostrarContingut();*/
		}
		return strAux;
	}
}
