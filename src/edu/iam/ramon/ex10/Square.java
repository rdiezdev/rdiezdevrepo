package edu.iam.ramon.ex10;

public class Square extends Rectangle {

	public Square() {
		super();
		this.length = this.width;
	}

	public Square(double side) {
		super(side, side);
	}

	public Square(double side, String color, boolean filled) {
		super(side, side, color, filled);
	}

	public double getSide() {
		return this.length;
	}

	public void setSide(double side) {
		this.length = side;
		this.width = side;
	}

	@Override
	public void setWidth(double width) {
		this.setSide(width);
	}

	@Override
	public void setLength(double length) {
		this.setSide(length);
	}

	@Override
	public String toString() {
		return "Square: " + color + " - " + filled + " - " + "Costat: " + this.width;
	}

}
