package edu.iam.ramon.ex10;

public class Rectangle extends Shape {
	
	protected double width;
	protected double length;
	
	public Rectangle() {
		super();
		this.length = 10.0;
		this.width = 5.0;
	}
	
	public Rectangle(double w, double l) {
		super();
		this.width = w;
		this.length = l;
	}
	
	public Rectangle(double w, double l, String color, boolean filled) {
		super(color, filled);
		this.width = w;
		this.length = l;
	}
	
	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getArea() {
		return this.width * this.length;
	}

	public double getPerimeter() {
		return this.width * 2 + this.length * 2;
	}

	public String toString() {
		return "Rectangle: " + color + " - " + filled + " - " + "Ample: " + this.width + " - Llarg: " + this.length;
	}

}
