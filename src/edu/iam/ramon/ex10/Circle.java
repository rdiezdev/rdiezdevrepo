package edu.iam.ramon.ex10;

public class Circle extends Shape {
	protected double radius;

	public Circle() {
		super();
		this.radius = 10.0;
	}

	public Circle(double radius) {
		super();
		this.radius = radius;
	}

	public Circle(String color, boolean filled, double radius) {
		super(color, filled);
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getArea() {
		return Math.PI * Math.pow(radius, 2);
	}

	public double getPerimeter() {
		return 2 * Math.PI * radius;
	}

	public String toString() {
		return "Cercle: " + color + " - " + filled + " - " + " Radi: " + radius;
	}

}
