package edu.iam.ramon.ex10;

public class Main {

	public static void main(String[] args) {
		
		//Què és el "downcast" o "upcast"? Quins dels exemples anteriors són una cosa o l'altre?
		/*
		*	- DownCast: Quan intentes convertir una clase superior en la jerarquia a una inferior.
		*		(De Vehicle a Cotxe)
		*		
		*	- UpCast: Quan intentes conertir una clase inferior en la jerarquia a una superior.
		*		(De Cotxe a Vehicle)
		*	
		*	Son UpCast: Exemple1, Exemple 3, Exemple 5, 
		*	Son DownCast: Exemple 2, Exemple 4, Exemple 6, Exemple 7
		*/
		
		//Exemple 1
		Shape s1 = new Circle("RED", false, 5.5);
		System.out.println(s1);                   
		System.out.println(s1.getArea());          
		System.out.println(s1.getPerimeter());    
		System.out.println(s1.getColor());
		System.out.println(s1.isFilled());
		//System.out.println(s1.getRadius()); **** No funciona ja que Shape, no te definit el mètode getRadius() 
		  
		//Exemple 2 
		Circle c1 = (Circle) s1; //Tot funciona ja que els mètodes s'han definit a shape o a circle                  
		System.out.println(c1);                  
		System.out.println(c1.getArea());
		System.out.println(c1.getPerimeter());
		System.out.println(c1.getColor());
		System.out.println(c1.isFilled());
		System.out.println(c1.getRadius());
		   
		//Exemple 3
		Shape s3 = new Rectangle(1.0, 2.0, "RED", false);  
		System.out.println(s3);
		System.out.println(s3.getArea());
		System.out.println(s3.getPerimeter());
		System.out.println(s3.getColor());
		//System.out.println(s3.getLength()); **** No funciona ja que Shape, no te definit el mètode getLength() 
		   
		//Exemple 4
		Rectangle r1 = (Rectangle)s3;  //Funciona tot ja que tot està definit a Rectangle o a una clase pare d'ella.
		System.out.println(r1);
		System.out.println(r1.getArea());
		System.out.println(r1.getColor());
		System.out.println(r1.getLength());
		   
		//Exemple 5
		Shape s4 = new Square(6.6);     //No funciona getSide() ja que Shape no te definit getSide() 
		System.out.println(s4);
		System.out.println(s4.getArea());
		System.out.println(s4.getColor());
		//System.out.println(s4.getSide());
		  
		//Exemple 6
		Rectangle r2 = (Rectangle)s4; //No funciona getSide() ja que no està definit ni a Shape ni a Rectangle
		System.out.println(r2);
		System.out.println(r2.getArea());
		System.out.println(r2.getColor());
		//System.out.println(r2.getSide());
		System.out.println(r2.getLength());
		
		// Exemple 7
		Square sq1 = (Square)r2;  //Tot funciona
		System.out.println(sq1);
		System.out.println(sq1.getArea());
		System.out.println(sq1.getColor());
		System.out.println(sq1.getSide());
		System.out.println(sq1.getLength());
		

	}

}
