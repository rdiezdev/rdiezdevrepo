package edu.iam.ramon.ex2;

public class Partit {
	private Equip equipLocal, equipVisitant;
	private int golsEquipLocal, golsEquipVisitant;
	
	public Partit(Equip eLocal, Equip eVisitant){
		this.equipLocal = eLocal;
		this.equipVisitant = eVisitant;
		this.golsEquipLocal=0;
		this.golsEquipVisitant=0;
	}
	
	public void marcaEquipLocal(){
		golsEquipLocal++;
	}
	
	public void marcaEquipVisitant(){
		golsEquipVisitant++;
	}
	
	public String fi(){
		if (golsEquipLocal>golsEquipVisitant)
			return equipLocal.toString();
		else if(golsEquipVisitant>golsEquipLocal)
			return equipLocal.toString();
		else
			return "empat";
	}

	@Override
	public String toString() {
		return "Equip Local: " + equipLocal.getNomEquip() + " gols: " + golsEquipLocal + "\nEquip Visitant: " + equipVisitant.getNomEquip() + " gols: " + golsEquipVisitant;
	}

	public int getGolsEquipLocal() {
		return golsEquipLocal;
	}

	public int getGolsEquipVisitant() {
		return golsEquipVisitant;
	}
	
}
