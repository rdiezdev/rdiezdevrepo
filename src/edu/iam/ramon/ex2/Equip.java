package edu.iam.ramon.ex2;

public class Equip {
	int puntsLliga;
	String nomEquip;
	
	public Equip(String nomE){
		this.nomEquip= nomE;
		this.puntsLliga=0;
	}
	
	@Override
	public String toString(){
		return nomEquip;
	}

	public int getPuntsLliga() {
		return puntsLliga;
	}

	public void setPuntsLliga(int puntsLliga) {
		this.puntsLliga = puntsLliga;
	}

	public String getNomEquip() {
		return nomEquip;
	}
	
	

}
