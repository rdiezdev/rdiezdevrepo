package edu.iam.ramon.ex2;

public class testPartit {
	public static void main(String Args[]){
		
		Equip equipLocal = new Equip("Barça");
		Equip equipVisitant = new Equip("Madrid");

		Partit p1 = new Partit(equipLocal, equipVisitant);
		p1.marcaEquipLocal();
		p1.marcaEquipVisitant();
		System.out.println(p1.fi());
		System.out.println(p1);
	}
}
