package edu.iam.ramon.ex6;

import java.util.Arrays;
import java.util.Random;

public class Lliga {
	private static int numEquips = 0;
	Equip[] equips;
	Partit[][] partits;

	public Lliga(Equip[] equips) {
		this.equips = equips;
		
		//Afegeixo el número d'equips
		numEquips = equips.length;
		
		partits = new Partit[equips.length][equips.length];
		for (int i = 0; i < equips.length; i++) {
			for (int j = 0; j < equips.length; j++) {
				if (i != j) {
					this.partits[i][j] = new Partit(equips[i], equips[j]);
				}
			}
		}
	}
	
	public static int getNumeroEquips() {
		return numEquips;
	}
	
	private void jugarPartit(Partit partit) {
		int gols;
		Random rnd = new Random();

		gols = rnd.nextInt(6);
		for (int i = 1; i <= gols; i++)
			partit.marcaEquipLocal();

		gols = rnd.nextInt(6);
		for (int i = 1; i <= gols; i++)
			partit.marcaEquipVisitant();

		partit.fi();
	}

	public void jugaLliga() {
		for (int i = 0; i < equips.length; i++) {
			for (int j = 0; j < equips.length; j++) {
				if (i != j) {
					jugarPartit(partits[i][j]);
				}
			}
		}
	}

	public String classificacio() {
		String clasi = "";
		Arrays.sort(equips);
		for (int i = 0; i < this.equips.length; i++) {
			clasi += equips[i].getNomEquip() + ": " + equips[i].getPuntsLliga() + "\n";
		}
		return clasi;
	}

}
