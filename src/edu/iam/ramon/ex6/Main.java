package edu.iam.ramon.ex6;

public class Main {

	public static void main(String[] args) {
		Equip equip1 = new Equip("Barca");
		Equip equip2 = new Equip("Madrid");
		Equip equip3 = new Equip("Betis");
		Equip equip4 = new Equip("Rayo");
		Equip equip5 = new Equip("Valencia");
		Equip[] equips = {equip1, equip2, equip3, equip4, equip5};
		Lliga lliga = new Lliga(equips);
		
		System.out.println("Hi ha " + Lliga.getNumeroEquips() + "equips a la lliga.");
		
		lliga.jugaLliga();
		
		System.out.println(lliga.classificacio());
		
	}

}
