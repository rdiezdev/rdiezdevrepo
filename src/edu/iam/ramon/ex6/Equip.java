package edu.iam.ramon.ex6;

public class Equip implements Comparable<Equip>{
	private int puntsLliga;
	private String nomEquip;
	
	public Equip(String nomE){
		this.nomEquip= nomE;
		this.puntsLliga=0;
	}
	
	@Override
	public String toString(){
		return nomEquip;
	}

	public int getPuntsLliga() {
		return puntsLliga;
	}

	public void setPuntsLliga(int puntsLliga) {
		this.puntsLliga += puntsLliga;
	}

	public String getNomEquip() {
		return nomEquip;
	}

	@Override
	public int compareTo(Equip o) {
		
		if(this.puntsLliga > o.puntsLliga)
			return -1;
		else if(this.puntsLliga < o.puntsLliga)
			return 0;
		return 0;
	}
	
	

}
